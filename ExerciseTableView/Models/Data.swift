//
//  Data.swift
//  ExerciseTableView
//
//  Created by Rahul Rawat on 10/06/21.
//

import Foundation

class DataModel {
    let text: String
    let index: Int
    
    init(text: String, index: Int) {
        self.text = text
        self.index = index
    }
}
