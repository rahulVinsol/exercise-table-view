//
//  TableViewCell.swift
//  ExerciseTableView
//
//  Created by Rahul Rawat on 02/06/21.
//

import UIKit

class CheckmarkAsset {
    static let shared: CheckmarkAsset = CheckmarkAsset()
    
    private init() { }
    
    let checkedImage: UIImage = {
        let image = UIImage(systemName: "checkmark.circle.fill")
        image?.withTintColor(.white)
        return image!
    }()

    let uncheckedImage: UIImage = {
        let image = UIImage(systemName: "circle")
        image?.withTintColor(.white)
        return image!
    }()
}

class TableViewCell: UITableViewCell {
    
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var indexLabel: UILabel!
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        if editing {
            reorderControlImageView?.tint(color: .white)
            changeCheckmark()
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        changeCheckmark()
    }
}

extension UITableViewCell {

    var reorderControlImageView: UIImageView? {
        let reorderControl = self.subviews.first { view -> Bool in
            view.classForCoder.description() == "UITableViewCellReorderControl"
        }
        return reorderControl?.subviews.first { view -> Bool in
            view is UIImageView
        } as? UIImageView
    }
    
    var checkMarkImageView: UIImageView? {
        let reorderControl = self.subviews.first { view -> Bool in
            view.classForCoder.description() == "UITableViewCellEditControl"
        }
        return reorderControl?.subviews.first { view -> Bool in
            view is UIImageView
        } as? UIImageView
    }
    
    func changeCheckmark() {
        checkMarkImageView?.image = self.isSelected ? CheckmarkAsset.shared.checkedImage : CheckmarkAsset.shared.uncheckedImage
    }
}

extension UIImageView {
    func tint(color: UIColor) {
        self.image = self.image?.withRenderingMode(.alwaysTemplate)
        self.tintColor = color
    }
}
