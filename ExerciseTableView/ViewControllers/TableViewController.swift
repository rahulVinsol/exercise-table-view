//
//  TableViewController.swift
//  ExerciseTableView
//
//  Created by Rahul Rawat on 02/06/21.
//

import UIKit

class TableViewController: UITableViewController {

    private var data: [DataModel] = {
        let strings = [
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean viverra risus vel ultrices imperdiet. Curabitur cursus est eget feugiat porttitor. Fusce ut lectus at diam dictum iaculis. Aliquam malesuada erat vitae lectus tempor eleifend.",
            "Mauris a mauris ac est tempus hendrerit at nec sapien Maecenas consequat ipsum ac fermentum ultricies.",
            "Ut eleifend neque sed massa ullamcorper aliquet. Sed quis ante at ligula eleifend tincidunt in vitae mauris. Sed non metus vitae ipsum convallis aliquet quis quis erat.",
            "Nunc lobortis est lobortis pulvinar porttitor.",
            "Ut a est nec ante vehicula efficitur. Nunc sed libero non eros tincidunt auctor.",
            "Cras gravida urna vitae arcu maximus, pulvinar rutrum purus interdum. Sed mattis purus ut nisi scelerisque rutrum. Donec pharetra justo feugiat diam porttitor, ut volutpat justo faucibus. Etiam non orci dapibus, sagittis magna quis, tincidunt odio.",
            "Nulla nec erat non lectus fermentum feugiat. Vestibulum dapibus orci in augue commodo luctus.",
            "Quisque consequat est sed fringilla venenatis.",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean viverra risus vel ultrices imperdiet.",
            "Curabitur cursus est eget feugiat porttitor. Fusce ut lectus at diam dictum iaculis.",
            "Aliquam malesuada erat vitae lectus tempor eleifend. Mauris a mauris ac est tempus hendrerit at nec sapien. Maecenas consequat ipsum ac fermentum ultricies. Ut eleifend neque sed massa ullamcorper aliquet. Sed quis ante at ligula eleifend tincidunt in vitae mauris. Sed non metus vitae ipsum convallis aliquet quis quis erat.",
            "Nunc lobortis est lobortis pulvinar porttitor. Ut a est nec ante vehicula efficitur.",
            "Nunc sed libero non eros tincidunt auctor.",
            "Cras gravida urna vitae arcu maximus, pulvinar rutrum purus interdum. Sed mattis purus ut nisi scelerisque rutrum. Donec pharetra justo feugiat diam porttitor, ut volutpat justo faucibus.",
            "Etiam non orci dapibus, sagittis magna quis, tincidunt odio. Nulla nec erat non lectus fermentum feugiat.",
            "Vestibulum dapibus orci in augue commodo luctus.",
            "Quisque consequat est sed fringilla venenatis.",
        ]
        
        var modelList = [DataModel]()
        
        for item in strings {
            modelList.append(DataModel(text: item, index: modelList.count + 1))
        }
        
        return modelList
    }()
    
    private var deleteButton: UIBarButtonItem!
    
    private lazy var insertionAlertVc: UIAlertController = {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        let alertVc = UIAlertController(title: "Insert New Row", message: nil, preferredStyle: .alert)

        alertVc.addTextField(configurationHandler: {(textField) in
            textField.placeholder = "Enter text"
        })

        alertVc.addAction(UIAlertAction(title: "Add", style: .default, handler: { [weak self] (alert) in
            guard let self = self else {
                return
            }
            let row = alertVc.textFields![0].text
            if let rowData = row {
                self.data.append(DataModel(text: rowData, index: self.data.count + 1))
                self.tableView.insertRows(at: [IndexPath(row: self.data.count - 1, section: 0)], with: .right)
                self.setEditButtonEnabledStatus()
            }
        }))

        alertVc.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        return alertVc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        
        self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        deleteButton = UIBarButtonItem(title: "Delete", style: .plain, target: self, action: #selector(deleteTapped))
        deleteButton.tintColor = .red
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        self.navigationItem.leftBarButtonItem = editing ? deleteButton : nil
        deleteButton.isEnabled = false
        
        super.setEditing(editing, animated: animated)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count + 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell

        let index = indexPath.row
        
        cell.indexLabel.isHidden = index >= data.count
        
        if index < data.count {
            let isEven = (index + 1) % 2 == 0
            
            let element = data[index]
            
            cell.backgroundView = UIImageView(image: UIImage(named: isEven ? "even" : "odd")!)
            
            cell.cellLabel.text = element.text
            cell.indexLabel.text = "\(element.index)"
            
            let textColor: UIColor = isEven ? .black : .white
            cell.cellLabel.textColor = textColor
            cell.indexLabel.textColor = textColor
        } else {
            cell.backgroundView = nil
            cell.cellLabel.textColor = .black
            cell.cellLabel.text = "Insert New Row"
            cell.indexLabel.text = ""
            
            cell.selectionStyle = .none
        }
        
        return cell
    }

    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        indexPath.row < data.count ? .delete : .none
    }

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            data.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            reloadTable(after: 0.5)
            changeDeleteButtonTitle()
            setEditButtonEnabledStatus()
        } else if editingStyle == .insert {
            showInsertionAlertVc()
        }
    }
    
    private func showInsertionAlertVc() {
        insertionAlertVc.textFields?[0].text = ""
        present(insertionAlertVc, animated: true, completion: nil)
    }
    
    private func reloadTable(after seconds: Double) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            self.tableView.reloadData()
        }
    }
    
    @objc private func reloadTable() {
        self.tableView.reloadData()
    }

    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        let tempRow = data[fromIndexPath.row]
        data.remove(at: fromIndexPath.row)
        data.insert(tempRow, at: to.row)
        reloadTable(after: 0.5)
    }

    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return indexPath.row < data.count
    }
    
    override func tableView(_ tableView: UITableView, targetIndexPathForMoveFromRowAt sourceIndexPath: IndexPath, toProposedIndexPath proposedDestinationIndexPath: IndexPath) -> IndexPath {
        if proposedDestinationIndexPath.row == data.count {
            return IndexPath(row: self.data.count - 1, section: 0)
        }
        return proposedDestinationIndexPath
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if isEditing {
            cell.reorderControlImageView?.tint(color: .white)
        }
    }
    
    @objc func deleteTapped(_ sender: UIButton) {
        //to sort the rows in decreasing order so that the rows are deleted from bottom up to prevent inconsistent deletion
        let sortedIndexes = tableView.indexPathsForSelectedRows?.sorted { $0.row > $1.row }
        
        if var selectedRows = sortedIndexes {
            var index = 0
            while index < selectedRows.count {
                let indexPath = selectedRows[index]
                if indexPath.row < data.count {
                    //to remove all the other selected rows
                    data.remove(at: indexPath.row)
                    index += 1
                } else {
                    //to remove insertion row in case it was selected
                    selectedRows.remove(at: index)
                }
            }
            
            tableView.beginUpdates()
            tableView.deleteRows(at: selectedRows, with: .automatic)
            tableView.endUpdates()
            
            changeDeleteButtonTitle()
            reloadTable(after: 0.5)
            setEditButtonEnabledStatus()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isEditing {
            if indexPath.row >= data.count {
                tableView.deselectRow(at: indexPath, animated: false)
            } else {
                changeDeleteButtonTitle()
            }
        } else if indexPath.row == data.count {
            showInsertionAlertVc()
        }
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        changeDeleteButtonTitle()
    }
    
    private func changeDeleteButtonTitle() {
        let selectedRowsCount = tableView.indexPathsForSelectedRows?.count ?? 0

        deleteButton.title = "Delete" + (selectedRowsCount > 0 ? " (\(selectedRowsCount))" : "")
        
        deleteButton.isEnabled = selectedRowsCount > 0
    }
    
    private func setEditButtonEnabledStatus() {
        editButtonItem.isEnabled = data.count > 0
        
        if isEditing {
            setEditing(false, animated: true)
        }
    }
}
